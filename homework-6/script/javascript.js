let arr = ['hello', 'world', 23, '23', null];


function filterBy(arr, type) {
    return arr.filter(function (item) {
        if( typeof item !== type){
            return true;
        }
    });
}

console.log(filterBy(arr , 'string'));