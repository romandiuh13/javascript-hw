const passwordForm = document.querySelector('.password-form')
const inputIntroduce = passwordForm.querySelector('.input-wrapper.introduce')
const inputConfirm = passwordForm.querySelector('.input-wrapper.confirm')
const introduceIconPassword = inputIntroduce.querySelector('.icon-password')
const confirmIconPassword = inputConfirm.querySelector('.icon-password')
const inputIntroducePassword = inputIntroduce.querySelector('.password')
const inputConfirmPassword = inputConfirm.querySelector('.password')

introduceIconPassword.addEventListener('click', function(){
    if(inputIntroducePassword.type === 'password'){
        inputIntroducePassword.type = 'text'
        introduceIconPassword.className = 'fas fa-eye icon-password'
    } else{
        inputIntroducePassword.type = 'password'
        introduceIconPassword.className = 'fas fa-eye-slash icon-password'
    }
})

confirmIconPassword.addEventListener('click', function(){
    if(inputConfirmPassword.type === 'password'){
        inputConfirmPassword.type = 'text'
        confirmIconPassword.className = 'fas fa-eye icon-password'
    } else {
        inputConfirmPassword.type = 'password'
        confirmIconPassword.className = 'fas fa-eye-slash icon-password'
    }
})

const button = passwordForm.querySelector('.btn')
button.addEventListener('click', function(){
     if(inputIntroducePassword.value === inputConfirmPassword.value){
         alert('You are welcome')
     } else {
        text = 'Нужно ввести одинаковые значения'
        inputConfirm.insertAdjacentHTML('afterend', `<span>${text}</span>`)
     }
})
