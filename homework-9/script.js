const tabList = document.querySelector('.centered-content .tabs')
const tabItems = tabList.querySelectorAll('.tabs-title')

tabItems.forEach((elem) => {
    elem.addEventListener('click', function(){
        const tabContainer = this.closest('.tabs')
        const allTab = tabContainer.querySelectorAll('.tabs-title')
        const tabItemsActive = tabContainer.querySelector('.tabs-title.active')
        tabItemsActive.classList.remove('active')
        this.classList.add('active')
        let index = 0;
        for(let i =0; i < allTab.length; i++){
            if(allTab[i] === this){
                index = i;
                break;
            }
        }
        const prevActiveContent = document.querySelector('.tabs-content-item.active');
        prevActiveContent.classList.remove('active')
        const activeTab = document.querySelectorAll('.tabs-content-item')[index];
        activeTab.classList.add('active')
    })
})