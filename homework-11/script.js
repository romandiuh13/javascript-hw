document.addEventListener('keydown', function(event){
    const buttons = document.querySelectorAll('.btn-wrapper .btn')
    buttons.forEach((elem) => {
        
        if(elem.textContent === event.key){
            const activeButton = document.querySelector('.btn.active')
            if(activeButton){
            activeButton.classList.remove('active')}
            elem.classList.add('active')
    }
    })
})