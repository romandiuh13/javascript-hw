// const root = document.querySelector(':root');
// const rootStyles = getComputedStyle(root);
// let mainColor = rootStyles.getPropertyValue('--color1');



const App = {
    createView: function (){
        this.container = document.getElementsByClassName( "container" )[0];
        this.item = document.getElementsByClassName( "item" )[0];
        this.state = false;
    },

    toggleIt: function ( ev ) {
        const root = document.querySelector(':root');
        // const rootStyles = getComputedStyle(root);
        // const mainColor = rootStyles.getPropertyValue('--color1');
        if( App.state ){
            App.item.style.backgroundColor = '#faaf40'
            App.item.style.left = "3px";
            App.item.style.animation= "moveL .4s";
            App.state = false;
            root.style.setProperty('--color1', '#faaf40')
        }else{
            App.item.style.backgroundColor = 'lightblue'
            App.item.style.left = "36px";
            App.item.style.animation= "move .5s";
            App.state = true;
            root.style.setProperty('--color1', 'lightblue')
            
        }
    },

    bindEv: function () {
        this.container.addEventListener( 'click', this.toggleIt );
    },

    init: function (){
        this.createView();
        this.bindEv();
    }
};
App.init();