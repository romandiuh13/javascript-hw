const slides = document.querySelectorAll('.image-to-show');
let currentSlide = 0;
const slideInterval = setInterval(nextSlide,2000);

function nextSlide() {
    slides[currentSlide].className = 'image-to-show';
    currentSlide = (currentSlide+1)%slides.length;
    slides[currentSlide].className = 'image-to-show show';
}

let playing = true;
const stopButton = document.querySelector('.button-stop');
function stopSlideShow(){
    stopButton.innerHTML = 'Начать показ';
    playing = false;
    clearInterval(slideInterval);
}

function playSlideShow(){
    stopButton.innerHTML = 'Остановить показ';
    playing = true
    slideInterval = setInterval(nextSlide, 2000);
}

stopButton.addEventListener('click', function(){
    if(playing){
        stopSlideShow();
    } else{
        playSlideShow()
    }
})