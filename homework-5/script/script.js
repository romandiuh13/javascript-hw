function createNewUser() {
    let name = prompt('Enter your name');
    let surName = prompt('Enter your surname');
    let usersBirthday = prompt('Enter your birthday','dd,mm,yyyy');
    let newUser = {
        firstName : name,
        lastName : surName,
        birthday : usersBirthday,

        getAge: function (usersBirth) {
            let today = new Date();
            let birthDate = new Date(usersBirth);
            let age = today.getFullYear() - birthDate.getFullYear();
            let m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age = age - 1;
            }
            return age;
        },
         getPassword : function () {
            return((this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(this.birthday.length - 4)))
         }
    };
    return newUser
}
const user = createNewUser();
console.log(user);
console.log(user.getAge(user.birthday));
console.log(user.getPassword());