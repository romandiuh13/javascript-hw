const inputPriceField = document.querySelector('.input-price');

inputPriceField.addEventListener('blur', () => {
    const checkCurrentMessage = document.querySelector('.price')

    if (checkCurrentMessage) {
        checkCurrentMessage.remove();
    }

    const priceContainer = document.createElement('div');
    const priceSpan = document.createElement('span');
    const deleteButton = document.createElement('button');
    const checkErrorMessage = document.querySelector('.errorSpan');
    const inputValue = Number(inputPriceField.value);

    if (!isNaN(Number(inputValue)) && inputPriceField.value !== '' && inputValue > 0) {
        priceContainer.classList.add('price');
        // priceSpan.classList.add('text');
        deleteButton.classList.add('delete-button');
        inputPriceField.classList.remove('input-error');
        
        if (checkErrorMessage) {
            checkErrorMessage.parentElement.remove()
        }

        priceSpan.innerText = `Current price ${inputPriceField.value}`;
        deleteButton.innerText = 'X';

        deleteButton.addEventListener('click', () => {
            priceContainer.remove();
            inputPriceField.value = '';
        });

        priceContainer.appendChild(priceSpan);
        priceContainer.appendChild(deleteButton);
        document.body.appendChild(priceContainer);

    } else {
        const correctSpan = document.createElement('span');

        inputPriceField.classList.add('input-error');

        if (checkErrorMessage) {
            checkErrorMessage.parentElement.remove()
        }


        correctSpan.classList.add('errorSpan');
        correctSpan.innerText = 'Please enter correct price';

        priceContainer.appendChild(correctSpan);
        document.body.appendChild(priceContainer);
    }
});