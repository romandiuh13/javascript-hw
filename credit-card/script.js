const creditCard = {
    pin: 1111,
    numberAttempts: 3,
    sum: 5000,
    creditLimit: 1000,
    status: 'active',
    getMoney: function () {
        let pinCode = +prompt('Please, Enter your pin code')
        

        if (this.status === 'active') {
            if (pinCode === this.pin && this.numberAttempts > 0) {
                const wantMoney = +prompt('How much money do you want?')

                if ((this.sum + this.creditLimit) > wantMoney) {
                    console.log(`Please, take the ${wantMoney}`)
                    this.sum -= wantMoney;
                    if (this.sum < 0) {
                        this.creditLimit += this.sum;
                        this.sum = 0;
                    }
                } else {
                    console.log('Insufficient funds')
                }
            } else {
                console.log('Your pin code is incorrect')
                if(this.numberAttempts === 0){
                    console.log('Your card is deactivated, please, contact to the bank')
                }
            }
        } else {
            console.log('Your card is deactivated, please, contact to the bank')
        }

    }
}
console.log(creditCard.getMoney())
console.log(creditCard)