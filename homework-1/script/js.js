let userName = prompt('Enter your name');
while (userName === null || userName.length === 0 || !isNaN(userName)){
    userName = prompt('Please, enter your name')
}

let userAge = +prompt('Enter your age');
while (userAge === null || isNaN(userAge) || userAge === "") {
    userAge = +prompt('Please, enter your age')
}

if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge >= 18 && userAge <= 21) {
    let check = confirm('Are you sure you want to continue?');
    if (check) {
        alert(`Welcome ${userName}`);
    } else {
        alert('You are not allowed to visit this website')
    }
} else {
    alert(`Welcome ${userName}`)
}